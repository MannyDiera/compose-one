#!/bin/bash

# Stop and remove container
docker stop proxy-container

docker rm proxy-container

# Optional to also remove image
# docker rmi proxy-image
