#!/bin/bash

# Stop and remove container
docker stop mysql-container

docker rm mysql-container

# Optional to also remove image
# docker rmi mysql-db-base
