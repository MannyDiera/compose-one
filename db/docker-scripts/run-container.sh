#!/bin/bash

# Run container, expose port 3306 from container to port 3308 on your machine, set root password for MySQL
docker run -p 3308:3306 --name mysql-container -d \
-e MYSQL_ROOT_PASSWORD=12345678 \
mysql-db-base
