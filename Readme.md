# Server Containerized Dependencies

This project allows us to run the dependencies used by our server with Docker and it includes the following:
- MySQL
- Proxy (With Nginx)
- Redis (In Progress/Need more info)

**Prerequisites**: You should have a datadump of the research db, ask any Architect for this data.

Users can run each service individually by running their corresponding Dockerfiles or as a group by using the docker-compose.yml file found at the root.

The initial goal is to run the server's dependencies only.

We could add the microservices to the docker-compose file also, however, we are keeping that as optional since the purpose of running services locally is to develop and debug and running them within containers would add a layer of complexity to these tasks.


## Docker Compose Instructions
1. In the root of this project, run the following commands:
```bash
# Start all the services in the docker-compose file
docker-compose up -d

# Restart one service
docker-compose restart <name-of-service>

# View logs for one service
docker-compose logs <name-of-service>

# For all services
docker-compose logs

# Stop one service
docker stop <service name>

# Stop all services
docker-compose stop

# Stop and remove all services
docker-compose down
```
