#!/bin/bash

# This will connect to the MySQL container and prompt you for the root password
docker exec -t -i mysql-container /bin/bash -c "mysql -uroot -p"
