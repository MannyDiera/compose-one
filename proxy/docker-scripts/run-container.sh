#!/bin/bash

# Run container, expose port 3030 on 3030
docker run -p 3030:3030 --name proxy-container \
--add-host=host.docker.internal:host-gateway \
proxy-image

