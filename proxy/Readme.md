# Nginx Proxy

We use this proxy as a local API Gateway, exposing only 1 port which is 3030 on localhost.

The client application can then point to http://locahost:3030 and this proxy will handle the routing to microservices also running on localhost.



Notes:

For the server blocks in the nginx.conf file, instead of this:
```bash
    location ^~ /api/v1/auth {
        ...
        proxy_pass http://localhost:8080/;
    }
```


We can do this:
```bash
    upstream auth {
        server 127.0.0.1:8080;
    }

...
   location ^~ /api/v1/auth {
        ...
        proxy_pass https://auth/;
   }

```
