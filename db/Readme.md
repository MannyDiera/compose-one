# MySQL 5.7.33 with Docker

## Instructions
1. Add the dump files next inside the init-data/ directory.

2. You can build the image and run the container in 2 ways. 
One is stand alone/by itself, and the other is alongside other services using Docker compose (instructions at root of project).

## Stand alone
Scripts have been added to the docker-scripts/ directory to make it easy to:
 - Build the image
 - Run the container
 - Connect to and stop/remove the container and image.

Run the .sh files from the db/ directory to build and run the container. 

```bash
# build image
docker-scripts/build-image.sh

# run as container
docker-scripts/run-container.sh
```

If you'd like to connect to the MySQL instance via the terminal/command line, run the following:
```bash
docker-scripts/connect.sh
```

Finally, when you'd like to stop and remove the container and image, run the following:
```bash
docker-scripts/clean-up.sh
```
